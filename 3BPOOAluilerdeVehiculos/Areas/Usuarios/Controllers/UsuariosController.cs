﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _3BPOOAluilerdeVehiculos.Areas.Usuarios.Controllers
{
    public class UsuariosController : Controller
    {
        [Area("Usuarios")]
        public IActionResult Usuarios()
        {
            return View();
        }
    }
}
